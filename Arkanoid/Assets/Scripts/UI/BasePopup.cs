﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(CanvasGroup))]
public class BasePopup : MonoBehaviour
{
	protected CanvasGroup		m_CanvasGroup;


	protected virtual void Awake()
	{
		m_CanvasGroup = GetComponent<CanvasGroup> ();
		m_CanvasGroup.alpha = 0;
		gameObject.SetActive (false);
	}


	protected IEnumerator FadeRoutine(float inc)
	{
		float targetValue = inc > 0 ? 1f : 0f;

		while (m_CanvasGroup.alpha != targetValue)
		{
			m_CanvasGroup.alpha = Mathf.Clamp (m_CanvasGroup.alpha + inc * Time.unscaledDeltaTime, 0, 1f);
			yield return new WaitForEndOfFrame();
		}

		if (inc < 0)
		{
			gameObject.SetActive (false);
		}
	}


	public void Open()
	{
		gameObject.SetActive (true);
		m_CanvasGroup.alpha = 0;
		StartCoroutine (FadeRoutine (5f));
		transform.SetAsLastSibling();
	}


	public void Close()
	{
		StartCoroutine (FadeRoutine (-5f));
	}
}
