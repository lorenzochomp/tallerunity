﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LoginMenu : BasePopup
{
	[SerializeField]
	private InputField		m_UsernameField;


	protected override void Awake()
	{
		base.Awake();
		m_UsernameField.onValidateInput += OnValidateInput;
	}


	private void OnDestroy()
	{
		m_UsernameField.onValidateInput -= OnValidateInput;
	}


	public char OnValidateInput(string text, int charIndex, char addedChar)
	{
		if (!char.IsLetterOrDigit(addedChar))
		{
			return '\0';
		}
		return addedChar;
	}


	public void OnCloseClick()
	{
		Close();
	}
}
