﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{
	[SerializeField]
	private SettingsMenu	m_SettingsPanel;
	[SerializeField]
	private LoginMenu		m_LoginMenu;



	public void OnPlayClick()
	{
		if (m_SettingsPanel.SelectedPhysicsMode == SettingsMenu.PhysicsMode.UNITY2D)
		{
			SceneManager.LoadScene("GamePhysics2D");
		}
		else
		{
			SceneManager.LoadScene("Game");
		}
	}


	public void OnSettingsClick()
	{
		m_SettingsPanel.Open();
	}


	public void OnLoginClick()
	{
		m_LoginMenu.Open();
	}
}
