﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class SettingsMenu : BasePopup
{
	public enum PhysicsMode
	{
		UNITY2D,
		CUSTOM
	}


	[SerializeField]
	private CreditsMenu		m_Credits;
	[SerializeField]
	private Dropdown		m_PhysicsModeDropdown;

	private bool			m_MusicIsOn;
	private bool			m_SfxIsOn;
	private Button			m_Close;
	private PhysicsMode		m_SelectedPhysicsMode;


	public PhysicsMode	SelectedPhysicsMode
	{
		get
		{
			return m_SelectedPhysicsMode;
		}
	}


	protected override void Awake()
	{
		base.Awake();

		// Restore the saved settings
		m_SelectedPhysicsMode = (PhysicsMode) PlayerPrefs.GetInt ("Arkanoid_Selected_Physics", 0);
		m_PhysicsModeDropdown.value = (int) m_SelectedPhysicsMode;
	}


	public void OnCloseClick()
	{
		Close();
	}


	public void OnCreditsClick()
	{
		m_Credits.Open();
	}


	public void OnMusicToggleChanged(bool newValue)
	{
		Debug.Log("OnMusicToggleChanged() --> isActive: " + newValue);
		m_MusicIsOn = newValue;
	}


	public void OnSfxToggleChanged(bool newValue)
	{
		Debug.Log("OnSfxToggleChanged() --> isActive: " + newValue);
		m_SfxIsOn = newValue;
	}


	public void OnPhysicsDropdownChanged (int value)
	{
		m_SelectedPhysicsMode = (PhysicsMode) value;

		// Save settings
		PlayerPrefs.SetInt ("Arkanoid_Selected_Physics", (int) m_SelectedPhysicsMode);

		Debug.Log ("Selected Physics Mode: " + m_SelectedPhysicsMode);
	}
}
