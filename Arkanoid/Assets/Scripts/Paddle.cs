﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
	[SerializeField]
	private Ball			m_Ball;

	private Vector3 		m_Velocity = new Vector3(10.0f, 0, 0);
	private Vector3			m_StartPos;
	private Vector3 		m_LastMousePos;
	private SpriteRenderer	m_Renderer;
	private Rigidbody2D		m_RigidBody;
	private bool			m_UseCustomPhysics;



	public Bounds Bounds
	{
		get
		{
			return m_Renderer.bounds;
		}
	}


	private void Awake ()
	{
		m_Renderer = GetComponent<SpriteRenderer>();
		m_RigidBody = GetComponent<Rigidbody2D>();
		m_StartPos = transform.position;
	}
	

	private void Move (int direction, float mult = 1f)
	{
		if (m_UseCustomPhysics)
		{
			transform.Translate(m_Velocity * direction * Time.deltaTime * mult);

			if (!m_Ball.IsLaunched)
			{
				m_Ball.ForceTranslate(m_Velocity * direction * Time.deltaTime * mult);
			}
		}
		else
		{			
			m_RigidBody.MovePosition (m_RigidBody.position + new Vector2(m_Velocity.x * direction * Time.fixedDeltaTime * mult, m_Velocity.y));

			if (!m_Ball.IsLaunched)
			{
				m_Ball.ForceTranslate(m_Velocity * direction * Time.fixedDeltaTime * mult);
			}
		}
	}


	public void Init (bool useCustomPhysics)
	{
		m_UseCustomPhysics = useCustomPhysics;
	}


	public void Reset()
	{
		if (m_UseCustomPhysics)
		{
			transform.position = m_StartPos;
		}
		else
		{
			m_RigidBody.position = m_StartPos;
		}
	}


	public void UpdateKeyboardInput ()
	{
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			Move (-1);
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
			Move (1);
		}
		else if (Input.GetKeyUp(KeyCode.Space))
		{
			m_Ball.Launch();
		}
	}


	public void UpdateMouseInput ()
	{
		Vector3 currentMousePos = Input.mousePosition;
		Vector3 dPos = currentMousePos - m_LastMousePos;

		if (dPos.sqrMagnitude > 0)
		{
			if (currentMousePos.x < m_LastMousePos.x)
			{
				Move(-1, 1.5f);
			}
			else if (currentMousePos.x > m_LastMousePos.x)
			{
				Move(1, 1.5f);
			}
		}
		if (Input.GetMouseButtonDown(0))
		{
			m_Ball.Launch();
		}
		m_LastMousePos = currentMousePos;	
	}
}
