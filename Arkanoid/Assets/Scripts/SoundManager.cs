﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;



public class SoundManager : MonoBehaviour
{
	private const int			MAX_AUDIO_SOURCES = 10;


	private static SoundManager 			m_Instance = null;
	private AudioSource[]					m_Sources;
	private AudioSource						m_CurrentMusicSource;
	private Dictionary<string, AudioClip>	m_ClipsCache = new Dictionary<string, AudioClip>();



	public static SoundManager Instance
	{
		get
		{
			if (m_Instance == null)
			{
				GameObject sndManager = new GameObject("SoundManager");
				m_Instance = sndManager.AddComponent<SoundManager>();
				m_Instance.Initialize();
				GameObject.DontDestroyOnLoad(sndManager);
			}
			return m_Instance;
		}
	}


	private void Initialize()
	{
		List<AudioSource> sources = new List<AudioSource>();
		for (int i = 0; i < MAX_AUDIO_SOURCES; i++)
		{
			sources.Add(gameObject.AddComponent<AudioSource>());
		}
		m_Sources = sources.ToArray();
	}


	private AudioSource GetAvailableAudioSource()
	{
		foreach (AudioSource source in m_Sources)
		{
			if (!source.isPlaying && source != m_CurrentMusicSource)
			{
				return source;
			}
		}
		return null;
	}


	private AudioSource PlayInternal(string name, bool loops, float volume = 1f)
	{
		AudioSource source = GetAvailableAudioSource();
		
		if (source)
		{
			source.loop = loops;
			source.clip = PreloadSound(name);
			source.volume = volume;
			source.Play();
		}
		return source;
	}


	public AudioSource PlaySfx(string name, bool loops = false)
	{
		return PlayInternal("Audio/Sfx/" + name, loops);
	}


	public AudioSource PlayMusic(string name, bool loops = true)
	{
		m_CurrentMusicSource = PlayInternal("Audio/Music/" + name, loops);
		return m_CurrentMusicSource;
	}


	public void StopMusic(bool fade = false)
	{
		if (m_CurrentMusicSource != null)
		{
			m_CurrentMusicSource.Stop();
		}
	}


	public void PauseMusic()
	{
		if (m_CurrentMusicSource != null)
		{
			m_CurrentMusicSource.Pause();
		}
	}


	public void ResumeMusic()
	{
		if (m_CurrentMusicSource != null)
		{
			m_CurrentMusicSource.UnPause();
		}
	}


	public AudioClip PreloadSound(string name)
	{
		AudioClip clip;
		if (!m_ClipsCache.ContainsKey(name))
		{
			clip = Resources.Load<AudioClip>(name);
			m_ClipsCache.Add(name, clip);
		}
		else
		{
			clip = m_ClipsCache[name];
		}
		return clip;
	}


	public void ClearCache()
	{
		foreach (KeyValuePair<string, AudioClip> keyPair in m_ClipsCache)
		{
			GameObject.Destroy(keyPair.Value);
		}
		m_ClipsCache.Clear();
	}
}
