﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class LevelManager : MonoBehaviour
{
	[SerializeField]
	private GameObject	m_BrickPrebab;

	private List<Brick>	m_Bricks;



	public Brick[] Bricks
	{
		get
		{
			return m_Bricks.ToArray();
		}
	}


	private void OnDestroy()
	{
		Clear();
	}


	public void CreateBricks(Bounds screenBounds)
	{
		m_Bricks = new List<Brick>();
		Vector2 brickSize = m_BrickPrebab.transform.GetComponent<Brick>().GetComponent<SpriteRenderer>().bounds.size;

		Vector3 screenTopLeftCorner = new Vector3(	screenBounds.center.x - screenBounds.extents.x + brickSize.x/2f, 
													screenBounds.center.y + screenBounds.extents.y - brickSize.y/2f);

		int rowsInStage = Mathf.FloorToInt (screenBounds.size.y / 3 / (brickSize.y));
		int colsInStage = Mathf.FloorToInt (screenBounds.size.x / (brickSize.x));

		for (int i = 0; i < rowsInStage; i++)
		{
			for (int j = 0; j < colsInStage; j++)
			{
				GameObject brickGo = GameObject.Instantiate(m_BrickPrebab);
				Brick brick = brickGo.GetComponent<Brick>();

				Vector3 brickPos = screenTopLeftCorner + new Vector3(j * brickSize.x, i * brickSize.y * -1);
				brick.transform.position = brickPos;
				brick.transform.SetParent (transform);
				brick.SetSprite (UnityEngine.Random.Range(0, 5));
				brick.name = "Brick_" + i + "_" + j;

				m_Bricks.Add(brick);
			}
		}
	}


	public void Clear()
	{
		if (m_Bricks != null && m_Bricks.Count > 0)
		{
			for(int i = 0; i < m_Bricks.Count; i++)
			{
				Destroy(m_Bricks[i].gameObject);
			}
			m_Bricks.Clear();
		}
	}
}
