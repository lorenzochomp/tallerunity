﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
	private enum GameState
	{
		PAUSE,
		PLAYING,
		GAME_OVER
	}


	private enum LimitersPosition
	{
		LEFT,
		RIGHT,
		TOP,
		BOTTOM
	}


	[SerializeField]
	private LevelManager	m_LevelManager;
	[SerializeField]
	private Ball			m_Ball;
	[SerializeField]
	private Paddle			m_Paddle;
	[SerializeField]
	private Text			m_ScoreText;
	[SerializeField]
	private Text			m_MsgText;
	[SerializeField]
	private bool			m_UseCustomPhysics;
	[SerializeField]
	private Transform[]		m_Limiters;


	private Brick[]			m_LevelBricks;
	private int				m_Score;
	private int				m_BricksDestroyedCount;
	private GameState		m_CurrentState;
	private Bounds			m_ScreenBounds;
	private float			m_EndGameTime;



	private void Awake()
	{
		// Compute the screen bounds
		float aspectRatio = (float)Screen.width / (float)Screen.height;
		float cameraHeight = Camera.main.orthographicSize * 2;
		float cameraWidth = cameraHeight * aspectRatio;
		m_ScreenBounds = new Bounds(Camera.main.transform.position, new Vector3(cameraWidth, cameraHeight, 0));

		if (!m_UseCustomPhysics)
		{
			Vector3 leftPos = new Vector3 (m_ScreenBounds.center.x - m_ScreenBounds.extents.x, 0, 0);
			Vector3 rightPos = new Vector3 (m_ScreenBounds.center.x + m_ScreenBounds.extents.x, 0, 0);

			m_Limiters[ (int)LimitersPosition.LEFT ].position = leftPos;
			m_Limiters[ (int)LimitersPosition.RIGHT ].position = rightPos;
		}
	}


	private void Start()
	{
		m_Ball.m_OnLost += OnBallLost;
		m_Ball.m_OnBrickDestroyed += OnBrickDestroyed;

		m_Ball.Init (m_Paddle, m_ScreenBounds, m_UseCustomPhysics);
		m_Paddle.Init (m_UseCustomPhysics);

		SoundManager.Instance.PlayMusic ("music");

		BeginGame();
	}


	private void OnDestroy()
	{
		m_Ball.m_OnLost -= OnBallLost;
		m_Ball.m_OnBrickDestroyed -= OnBrickDestroyed;
	}


	private void BeginGame()
	{
		m_LevelManager.CreateBricks (m_ScreenBounds);
		m_LevelBricks = m_LevelManager.Bricks;

		m_Ball.Bricks = m_LevelBricks;
		m_Ball.Reset();
		m_Paddle.Reset();

		m_MsgText.enabled = false;
		m_ScoreText.text = "0";
		m_Score = 0;
		m_BricksDestroyedCount = 0;
		m_CurrentState = GameState.PLAYING;

		if (m_UseCustomPhysics)
		{
			StartCoroutine (UpdatePhysicsRoutine());
		}
	}


	private void Update()
	{
		if (m_CurrentState == GameState.PLAYING)
		{
			if (m_UseCustomPhysics)
			{
				m_Paddle.UpdateKeyboardInput();
				//m_Paddle.UpdateMouseInput();
				m_Ball.UpdatePosition();
			}
			if (m_BricksDestroyedCount == m_LevelBricks.Length)
			{
				EndGame(true);
			}
		}
		else if (m_CurrentState == GameState.GAME_OVER)
		{
			if (Time.time > m_EndGameTime + 2.0f)
			{
				m_LevelManager.Clear();
				BeginGame();
			}
		}
	}

	// Used only for Unity's Physics2D
	private void FixedUpdate()
	{
		if (!m_UseCustomPhysics)
		{
			m_Paddle.UpdateKeyboardInput();
		}
	}


	private IEnumerator UpdatePhysicsRoutine()
	{
		while (m_CurrentState == GameState.PLAYING)
		{
			m_Ball.UpdatePhysics ();

			yield return new WaitForSeconds(0.1f);
		}
	}


	private void EndGame(bool hasWon)
	{
		m_CurrentState = GameState.GAME_OVER;
		m_MsgText.enabled = true;
		m_MsgText.text = hasWon ? "You Win!" : "You Lose!";
		m_EndGameTime = Time.time;

		SoundManager.Instance.PlaySfx(hasWon? "won" : "lose");
	}


	private void OnBrickDestroyed()
	{
		m_BricksDestroyedCount++;
		m_Score += 10;
		m_ScoreText.text = m_Score.ToString();
	}


	private void OnBallLost()
	{
		Debug.Log("OnBallLost");
		EndGame(false);
	}


	public void OnExitClick()
	{
		SceneManager.LoadScene("Main");
	}
}
