﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
	[SerializeField]
	private Sprite[]		m_Sprites;

	private SpriteRenderer	m_Renderer;
	private Collider2D		m_Collider;



	public Bounds Bounds
	{
		get
		{
			return m_Renderer.bounds;
		}
	}


	public Vector3 Size
	{
		get
		{
			return m_Renderer.bounds.size;
		}
	}


	private void Awake()
	{
		m_Renderer = GetComponent<SpriteRenderer>();
		m_Collider = GetComponent<Collider2D>();

	}


	public void SetSprite (int colorIdx)
	{
		m_Renderer.sprite = m_Sprites [Mathf.Clamp(colorIdx, 0, m_Sprites.Length-1)];
	}


	public void Show(bool isShown)
	{
		m_Renderer.enabled = isShown;
		m_Collider.enabled = isShown;
	}


}
