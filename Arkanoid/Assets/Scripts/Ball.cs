﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class Ball : MonoBehaviour
{
	public const float			SPEED = 5.0f;

	public event Action			m_OnLost;
	public event Action			m_OnBrickDestroyed;

	private Vector3				m_Velocity = Vector2.zero;
	private Vector3				m_PrevPos;
	private Vector3				m_StartPos;
	private SpriteRenderer		m_Renderer;
	private Rigidbody2D			m_RigidBody;
	private bool				m_IsLaunched;
	private bool				m_UseCustomPhysics;

	// Used for collision detection
	private Bounds				m_ScreenBounds;
	private Paddle				m_Paddle;
	private Brick[]				m_Bricks;

	[SerializeField]
	private Collider2D			m_BottomCollider;



	public Bounds Bounds
	{
		get
		{
			return m_Renderer.bounds;
		}
	}


	public Brick[] Bricks
	{
		get
		{
			return m_Bricks;
		}
		set
		{
			m_Bricks = value;
		}
	}


	public bool IsLaunched
	{
		get
		{
			return m_IsLaunched;
		}
	}


	private void Awake()
	{
		m_Renderer = GetComponent<SpriteRenderer>();
		m_RigidBody = GetComponent<Rigidbody2D>();
		m_StartPos = m_PrevPos = transform.position;
	}


	private void BounceV()
	{
		m_Velocity.y *= -1;
	}


	private void BounceH()
	{
		m_Velocity.x *= -1;
	}


	private void CheckBrickCollisions()
	{
		for (int i = 0; i < m_Bricks.Length; i++)
		{
			if (m_Bricks[i].enabled && m_Renderer.bounds.Intersects (m_Bricks[i].Bounds))
			{
				m_Bricks[i].Show(false);
				m_Bricks[i].enabled = false;

				Vector3 diffPos = m_Bricks[i].Bounds.center - transform.position; 

				if (Mathf.Abs (diffPos.x) > Mathf.Abs (diffPos.y))
				{
					BounceV();
				}
				else
				{
					BounceH();
				}
				transform.position = m_PrevPos;

				SoundManager.Instance.PlaySfx("pop");

				if (m_OnBrickDestroyed != null)
				{
					m_OnBrickDestroyed();
				}
				break;
			}
		}
	}


	private void CheckPaddleCollision()
	{
		if (m_Renderer.bounds.Intersects (m_Paddle.Bounds) && m_IsLaunched)
		{
			SoundManager.Instance.PlaySfx("pong");

			Vector3 diffPos = m_Paddle.Bounds.center - transform.position; 

			if (Mathf.Abs (diffPos.x) > Mathf.Abs (diffPos.y))
			{
				BounceV();
			}
			else
			{
				BounceV();
				BounceH();
			}
			transform.position = m_PrevPos;
		}
	}


	private void CheckWallsCollisions()
	{
		if (transform.position.x >= m_ScreenBounds.max.x || transform.position.x < m_ScreenBounds.min.x)
		{
			SoundManager.Instance.PlaySfx("pong");
			BounceH();
			transform.position = m_PrevPos;
		}
		else if (transform.position.y >= m_ScreenBounds.max.y)
		{
			SoundManager.Instance.PlaySfx("pong");
			BounceV();
			transform.position = m_PrevPos;
		}
		else if (transform.position.y < m_ScreenBounds.min.y)
		{
			m_Velocity = Vector3.zero;

			if ( m_OnLost != null)
			{
				m_OnLost();
			}
		}
	}


	private void OnCollisionEnter2D(Collision2D col)
	{
		if (!m_UseCustomPhysics && m_IsLaunched)
		{
			// The ball has collided with the paddle, let's add some impulse!
			if (col.gameObject == m_Paddle.gameObject)
			{
				int direction = UnityEngine.Random.Range(0, 2);
				Vector2 force = new Vector2(0.5f * ((direction == 0)? -1 : 1), 1.0f) * 2.5f;
				m_RigidBody.AddForce (force, ForceMode2D.Impulse);

				SoundManager.Instance.PlaySfx("pong");
			}
			// The ball is lost!
			else if (col.collider == m_BottomCollider)
			{				
				m_RigidBody.bodyType = RigidbodyType2D.Kinematic;
				m_RigidBody.Sleep();

				if ( m_OnLost != null)
				{
					m_OnLost();
				}
			}
			else
			{
				Brick brickComp = col.gameObject.GetComponent<Brick>();
				if (brickComp)
				{
					brickComp.Show(false);
					brickComp.enabled = false;
					SoundManager.Instance.PlaySfx("pop");

					if (m_OnBrickDestroyed != null)
					{
						m_OnBrickDestroyed();
					}
				}
			}				
		}
	}


	public void Init (Paddle paddle, Bounds screenBounds, bool useCustomPhysics = true)
	{		
		m_Paddle = paddle;
		m_ScreenBounds = screenBounds;
		m_UseCustomPhysics = useCustomPhysics;
	}


	public void Reset()
	{
		m_IsLaunched = false;

		if (m_UseCustomPhysics)
		{
			transform.position = m_StartPos;
		}
		else
		{
			m_RigidBody.position = m_StartPos;
			m_RigidBody.bodyType = RigidbodyType2D.Kinematic; 
		}
	}


	public void Launch()
	{
		if (!m_IsLaunched)
		{
			m_IsLaunched = true;
			int direction = UnityEngine.Random.Range(0, 2);

			if (m_UseCustomPhysics)
			{
				m_Velocity.x = SPEED * ((direction == 0)? -1 : 1);
				m_Velocity.y = SPEED;
			}
			else
			{
				m_RigidBody.bodyType = RigidbodyType2D.Dynamic;
				Vector2 force = new Vector2(0.5f * ((direction == 0)? -1 : 1), 1.0f) * SPEED * 2.5f;
				m_RigidBody.AddForce (force, ForceMode2D.Impulse);
			}
		}
	}


	public void ForceTranslate(Vector3 translation)
	{
		if (m_UseCustomPhysics)
		{
			transform.Translate (translation);
		}
		else
		{
			m_RigidBody.MovePosition (m_RigidBody.position + new Vector2(translation.x, translation.y));
		}
	}


	public void UpdatePosition()
	{
		if (m_UseCustomPhysics)
		{
			m_PrevPos = transform.position;
			transform.position += m_Velocity * Time.deltaTime;

			Vector3 pos = transform.position;
			pos.x += m_Velocity.x * Time.deltaTime;
			transform.position = pos;	
		}
	}


	public void UpdatePhysics()
	{
		CheckBrickCollisions();
		CheckPaddleCollision();
		CheckWallsCollisions();
	}
}
