﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	[SerializeField]
	Player player;
	[SerializeField]
	HealthBar healthBar;
	[SerializeField]
	Counter coins;
	[SerializeField]
	Counter keys;
	[SerializeField]
	Message message;
	[SerializeField]
	Doors doors;
	[SerializeField]
	ParticleSystem endLevelParticles;
	[SerializeField]
	GameObject pausePanel;

	Health playerHealth;

	public void OnDoorOpened() {
		message.Text = "Ganaste!";
		endLevelParticles.gameObject.SetActive(true);
		player.GetComponent<SimpleCharacterControl>().enabled = false;
	}

	public void OnPlayerDeath() {
		message.Text = "Perdiste... :(\nR para reiniciar";
		player.GetComponent<SimpleCharacterControl>().enabled = false;
	}

	public void OnPlayerTookDamage(float percent) {
		healthBar.SetHealthPercent(percent);
	}

	void Start() {
		playerHealth = player.GetComponent<Health>();
		doors.enabled = false;
		endLevelParticles.gameObject.SetActive(false);
		pausePanel.SetActive(false);
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.H))
			playerHealth.TakeDamage(5);

		if (playerHealth.IsDead && Input.GetKeyDown(KeyCode.R))
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void OnCoinCollected() {
		coins.Count = player.CoinsCount;
	}

	public void OnKeyCollected() {
		keys.Count = 1;
		doors.enabled = true;
	}

	public void Pause() {
		Time.timeScale = 0;
		pausePanel.SetActive(true);
	}

	public void UnPause() {
		Time.timeScale = 1;
		pausePanel.SetActive(false);
	}

	public void QuitGame() {
		Debug.Log("Closing game");
		Application.Quit();
	}
}
