﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	[SerializeField]
	Vector3 viewDirection;
	[SerializeField]
	Vector3 viewSource;
	[SerializeField]
	float attackCooldown = 2;
	[SerializeField]
	bool debugRaycast = false;

	Animator anim;
	float timeUntilNextAttack;

	void Awake() {
		anim = GetComponent<Animator>();
	}

	void Update () {
		if (timeUntilNextAttack <= 0) {
			Vector3 rayDirection = transform.TransformDirection(viewDirection);
			Vector3 rayOrigin = transform.TransformPoint(viewSource);
			Ray ray = new Ray (rayOrigin, rayDirection);
			float raycastDistance = 1;

			if (debugRaycast)
				Debug.DrawLine (ray.origin, ray.GetPoint (raycastDistance));

			if (Physics.Raycast (ray, raycastDistance, 1 << LayerMask.NameToLayer("Player"))) {
				anim.SetTrigger("Attack");
				timeUntilNextAttack = attackCooldown;
			}
		} else
			timeUntilNextAttack -= Time.deltaTime;
	}
}
