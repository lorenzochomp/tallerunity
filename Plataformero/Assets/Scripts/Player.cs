﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour {
	static readonly Vector3 centerOffset = new Vector3 (0, 1, 0);

	[SerializeField]
	bool debugRaycast = false;
	[SerializeField]
	UnityEvent OnCoinCollected;
	[SerializeField]
	UnityEvent OnKeyCollected;

	int coinsCount = 0;
	Dialog dialog;
	Health health;

	public int CoinsCount {
		get {
			return coinsCount;
		}
	}

	void Awake() {
		dialog = GetComponentInChildren<Dialog>();
		health = GetComponent<Health>();
	}

	void OnTriggerEnter(Collider other) {
		if (other.CompareTag ("Coin")) {
			coinsCount++;
			OnCoinCollected.Invoke();
			//TODO show particles
			Destroy(other.gameObject);
		}

		if (other.CompareTag("Key")) {
			OnKeyCollected.Invoke();
			//TODO show particles
			Destroy(other.gameObject);
		}

		if (other.CompareTag("Enemy")) {
			health.TakeDamage(20);
		}
	}

	void OnTriggerStay(Collider other) {
		if (other.CompareTag("Fire"))
			health.TakeDamage(5*Time.deltaTime);
	}

	void Update() {
		RaycastHit hitInfo;
		Ray ray = new Ray (transform.position + centerOffset, transform.forward);
		float raycastDistance = 1;

		if (debugRaycast)
			Debug.DrawLine (ray.origin, ray.GetPoint (raycastDistance));
		
		bool displayDialog = false;

		if (Physics.Raycast (ray, out hitInfo, raycastDistance, 1 << LayerMask.NameToLayer("Interactable"))) {
			Interactable interactable = hitInfo.collider.GetComponent<Interactable>();

			if (interactable.CanInteract()) {
				displayDialog = true;

				if (Input.GetKeyDown(KeyCode.E))
					interactable.Interact();
			}
		}

		if (dialog.IsOn != displayDialog)
			dialog.Toggle();
	}
}
