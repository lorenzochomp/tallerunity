﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class OnTookDamageEvent : UnityEvent<float> {}

public class Health : MonoBehaviour {

	[SerializeField]
	float maxHealth = 100;
	[SerializeField]
	UnityEvent OnDeath;
	[SerializeField]
	OnTookDamageEvent OnTookDamage;

	float currentHealth;

	public bool IsDead {
		get {
			return currentHealth == 0;
		}
	}

	void Awake() {
		currentHealth = maxHealth;
	}

	public void TakeDamage(float hp) {
		currentHealth -= hp;
		if (currentHealth <= 0) {
			currentHealth = 0;
			OnDeath.Invoke();
		}

		OnTookDamage.Invoke(GetHealthPercent());
	}

	public float GetHealthPercent() {
		return currentHealth / maxHealth;
	}

}
