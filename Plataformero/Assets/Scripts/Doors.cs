﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class OnDoorOpenEvent : UnityEvent {}

public class Doors : Interactable {

	[SerializeField]
	OnDoorOpenEvent OnDoorOpen;

	bool areOpen;
	Animator animator;
	Collider col;

	void Awake() {
		animator = GetComponent<Animator> ();
		col = GetComponent<Collider>();
	}

	void OnEnable() {
		col.enabled = true;
	}

	void OnDisable() {
		col.enabled = false;
	}

	public override bool CanInteract() {
		return !areOpen;
	}

	public override void Interact() {
		areOpen = true;
		animator.SetTrigger ("open");
		OnDoorOpen.Invoke();
	}

	void DoorOpeningEvent() {
		Debug.Log("Puerta abriéndose");
	}

	void DoorOpeningEventInt(int value) {
		Debug.Log("Puera abierta: " + value);
	}

	void DoorOpeningEventString(string value) {
		Debug.Log("Puera abierta: " + value);
	}

}
