﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : Interactable {

	[SerializeField]
	GameObject itemPrefab;

	bool isOpen;
	Animator animator;

	void Awake() {
		animator = GetComponent<Animator> ();
	}

	public override void Interact() {
		isOpen = true;
		animator.SetTrigger ("open");
		StartCoroutine(SpawnItemCoroutine());
	}

	public override bool CanInteract() {
		return !isOpen;
	}

	IEnumerator SpawnItemCoroutine() {
		yield return new WaitForSeconds(0.5f);
		
		GameObject item = Instantiate<GameObject> (itemPrefab,transform.position,Quaternion.identity);
		Vector3 forceDirection = (transform.forward + transform.up*3 + transform.right).normalized;

		item.GetComponent<Rigidbody>().AddForce (forceDirection*8,ForceMode.Impulse);
		Collider[] allColliders = item.GetComponents<Collider>();

		foreach(Collider col in allColliders)
			col.enabled = false;

		yield return new WaitForSeconds(0.5f);

		foreach(Collider col in allColliders)
			col.enabled = true;
	}
}
