﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraMode {
	Fixed3D,
	Isometric,
	TopDown
}

[RequireComponent(typeof(Camera), typeof(CameraFollow))]
public class CameraModeSwitch : MonoBehaviour {

	[SerializeField]
	CameraMode type = CameraMode.Fixed3D;
	[Header("Fixed3D")]
	[SerializeField]
	Vector3 fixed3DOffset = new Vector3(0,8,-12);
	[SerializeField]
	Vector3 fixed3DRotation = new Vector3(30,0,0);
	[Header("Isometric")]
	[SerializeField]
	Vector3 isometricOffset = new Vector3(-10,8,-15);
	[SerializeField]
	Vector3 isometricRotation = new Vector3(25,45,0);
	[Header("TopDown")]
	[SerializeField]
	Vector3 topDownOffset = new Vector3(0,9,0);
	[SerializeField]
	Vector3 topDownRotation = new Vector3(90,0,0);

	Camera targetCamera;
	CameraFollow targetCameraFollow;

	void Awake() {
		targetCamera = GetComponent<Camera>();
		targetCameraFollow = GetComponent<CameraFollow>();
	}

	void Start() {
		switch(type) {
			case CameraMode.Fixed3D:
				SetCameraValues(false, fixed3DOffset, fixed3DRotation);
				break;
			case CameraMode.Isometric:
				SetCameraValues(true, isometricOffset, isometricRotation);
				break;
			case CameraMode.TopDown:
				SetCameraValues(false, topDownOffset, topDownRotation);
				break;
		}
	}

	void SetCameraValues(bool isOrtographic, Vector3 offset, Vector3 rotation) {
		targetCamera.orthographic = isOrtographic;
		targetCameraFollow.SetOffset(offset);
		transform.eulerAngles = rotation;
		//transform.rotation = Quaternion.Euler(rotation);
	}
}
