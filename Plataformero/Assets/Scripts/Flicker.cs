﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class Flicker : MonoBehaviour {

	Light targetLight;
	float elapsedTime;

	void Awake() {
		targetLight = GetComponent<Light>();
	}

	void Update () {
		elapsedTime += Time.deltaTime;

		if (elapsedTime > 0.1) {
			targetLight.intensity = Random.Range(8,20);
			elapsedTime = 0;
		}
	}
}
