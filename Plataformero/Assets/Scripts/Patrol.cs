﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour {

	[SerializeField]
	Vector3[] patrolPoints;
	[SerializeField]
	float speed;

	int targetPatrolPoint = 0;
	
	Vector3 from;
	Vector3 to;
	float distance;

	float segmentTime;

	void Start() {
		NextPoint();
	}

	void Update() {
		segmentTime += Time.deltaTime;
		if (segmentTime * speed / distance < 1) {
			transform.position = Vector3.Lerp(from, to, segmentTime * speed / distance);
		} else
			NextPoint();
	}

	void NextPoint() {
		segmentTime = 0;
		transform.position = patrolPoints[targetPatrolPoint];
		from = transform.position;
		targetPatrolPoint++;
		targetPatrolPoint %= patrolPoints.Length;
		to = patrolPoints[targetPatrolPoint];
		distance = (from - to).magnitude;
		transform.LookAt(to);
	}
	
}
