﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour {

	public abstract bool CanInteract();
	public abstract void Interact();

}
