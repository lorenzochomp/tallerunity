﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Message : MonoBehaviour {

	Text text;
	
	public string Text {
		set {
			text.text = value;
			gameObject.SetActive(!string.IsNullOrEmpty(value));
		}
	}

	void Awake() {
		text = GetComponentInChildren<Text>();
		Text = string.Empty;
	}
	
}
