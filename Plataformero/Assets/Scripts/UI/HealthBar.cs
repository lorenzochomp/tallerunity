﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

	float maxWidth;
	RectTransform fillRectTransform;

	void Awake() {
		RectTransform rectTransform = GetComponent<RectTransform>();
		maxWidth = rectTransform.rect.width;
		fillRectTransform = transform.GetChild(0).GetComponent<RectTransform>();
	}

	public void SetHealthPercent(float percent) {
		/*Vector2 size = fillRectTransform.sizeDelta;
		size.x = -maxWidth*(1-percent);
		fillRectTransform.sizeDelta = size;*/
		fillRectTransform.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, maxWidth * percent);
	}
	
}
