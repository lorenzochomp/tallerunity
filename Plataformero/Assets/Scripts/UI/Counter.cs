﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour {

	Text count;

	public int Count {
		set {
			count.text = value.ToString();
		}
	}

	void Awake() {
		count = GetComponentInChildren<Text>();
	}
	
}
