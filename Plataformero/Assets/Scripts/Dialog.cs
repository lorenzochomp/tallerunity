﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialog : MonoBehaviour {

	Transform cameraTransform;

	public bool IsOn {
		get {
			return gameObject.activeSelf;
		}
	}
	
	void Start() {
		cameraTransform = Camera.main.transform;
	}
	
	void Update () {
		if (gameObject.activeSelf)
			transform.LookAt(cameraTransform,Vector3.up);
	}

	public void Toggle() {
		gameObject.SetActive(!gameObject.activeSelf);
	}
}
