﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	[SerializeField]
	Transform target;
	[SerializeField]
	float multiplier = 1;

	Vector3 offset = Vector3.zero;

	void Start() {
		transform.position = target.position + offset;
	}

	void Update() {
		transform.position = Vector3.Lerp(transform.position,target.position + offset,Time.deltaTime*multiplier);
	}

	public void SetOffset(Vector3 newOffset) {
		offset = newOffset;
	}

}
