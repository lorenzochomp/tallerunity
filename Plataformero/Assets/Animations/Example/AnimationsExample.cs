﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsExample : MonoBehaviour {

	[NonSerialized]
	public bool IsIdle = true;

	Animator anim;
	CapsuleCollider col;

	float defaultRadius;

	[SerializeField]
	Rigidbody ballRB;
	[SerializeField]
	Transform servingHandTr;

	int WidthHash;

	void Awake() {
		anim = GetComponent<Animator>();
		col = GetComponent<CapsuleCollider>();
		defaultRadius = col.radius;

		ballRB.gameObject.SetActive(false);

		WidthHash = Animator.StringToHash("Width");
	}

	void Update () {
		if (Input.GetKeyDown(KeyCode.Space) && IsIdle) {
			anim.SetTrigger("Attack");
		}

		if (Input.GetKeyDown(KeyCode.D) && IsIdle) {
			anim.SetTrigger("Die");
		}

		if (Input.GetKeyDown(KeyCode.S) && IsIdle) {
			anim.SetTrigger("Serve");
			ballRB.transform.SetParent(servingHandTr);
			ballRB.gameObject.SetActive(true);
			ballRB.transform.localPosition = -transform.right * 0.005f;
		}

		float Width = anim.GetFloat(WidthHash);
		col.radius = defaultRadius * Width;
	}

	public void Serve() {
		ballRB.transform.parent = null;
		ballRB.isKinematic = false;
		ballRB.AddForce(-transform.right * 5, ForceMode.Impulse);
	}

	public void ServeFinished() {
		StartCoroutine(ServeFinishedCoroutine());
	}

	IEnumerator ServeFinishedCoroutine() {
		yield return new WaitForSeconds(2);
		ballRB.isKinematic = true;
		ballRB.gameObject.SetActive(false);
	}
}
