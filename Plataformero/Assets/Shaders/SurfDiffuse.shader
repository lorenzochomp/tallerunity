﻿Shader "Taller/Surface Diffuse"
{
    Properties
    {
      _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
      Tags { "RenderType" = "Opaque" }

      CGPROGRAM
      #pragma surface surf Lambert

      uniform sampler2D _MainTex;


      struct Input
      {
          float2 uv_MainTex;
      };


      void surf (Input IN, inout SurfaceOutput o)
      {
          o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }