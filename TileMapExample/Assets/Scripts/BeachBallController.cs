﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeachBallController : MonoBehaviour
{
	private Rigidbody2D	m_RigidBody;
	private Vector3		m_StartPosition;



	private void Awake()
	{
		m_RigidBody = GetComponent<Rigidbody2D>();
		m_StartPosition = transform.position;
	}


	private void OnMouseDown()
	{
		Vector2 force = new Vector2( Random.Range(2f, 10f), Random.Range(2f, 10f));
		m_RigidBody.AddTorque (1, ForceMode2D.Impulse);
		m_RigidBody.AddForce (force, ForceMode2D.Impulse);
	}


	private void Update()
	{
		if (Input.GetKeyUp (KeyCode.R))
		{
			m_RigidBody.position = m_StartPosition;
		}
	}
}
